#!/bin/sh
apt-get update
apt-get install -y apt-utils
apt-get install -y software-properties-common
apt-get install -y build-essential
apt-get install -y curl
apt-get install -y wget
add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"
wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
apt-get install -y postgresql-9.6
apt-get install -y libssl-dev libreadline-dev zlib1g-dev
apt-get install -y libxslt-dev libxml2-dev
apt-get install -y libmagickwand-dev imagemagick
apt-get install -y libpq5 libpq-dev
